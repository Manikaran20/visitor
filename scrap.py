from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
import time
import pandas as pd

options = Options()

options.headless = True #Keep this true if using an account for the first time, if the code crashes or gets stuck, make it False to see To see the firefox window so that you know why and where is it crashing.

def getprofiles(username, password):
    
    driver= webdriver.Firefox(options=options)

    driver.get('https://www.linkedin.com')
    
    driver.find_element_by_class_name("nav__button-secondary").click() # click on the sign up buttom
    
    driver.implicitly_wait(2)
    
    driver.find_element_by_id("username").send_keys(username) #send username
    
    driver.find_element_by_id("password").send_keys(password) #send password
    
    driver.find_element_by_class_name("login__form_action_container").click() #click submit
    
    time.sleep(120) #Enough time to complete the security check
    
    Names=[]
    
    Position=[]
    
    Address=[]
    
    for keyword in ['CEO', 'Founder', 'COO', 'CPO', 'CSO', 'CXO', 'IT Head', 'Operation Manager', 'HR Head']:
    	driver.get(f'https://www.linkedin.com/search/results/people/?keywords={keyword}&origin=SWITCH_SEARCH_VERTICAL')
    
    	flag=True
    
    	while flag:
    
    		driver.execute_script("window.scrollTo(0,document.body.scrollHeight)") #Scroll down so that the next button gets clickable.
    		time.sleep(3) 
    
    		links=driver.find_elements_by_css_selector('a.search-result__result-link.ember-view') #profile links
    		profiles=[link.get_attribute('href') for link in links[::2]]
    		
    		if len(list(filter(lambda x: len(x)>85, profiles)))>=1: # If length of a linkedin profile address is this big, it means that profile is deactivated
    			
    			try:
    				element=WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//button[@class='artdeco-pagination__button artdeco-pagination__button--next artdeco-button artdeco-button--muted artdeco-button--icon-right artdeco-button--1 artdeco-button--tertiary ember-view']")))
    				element.click() #click the next button
    				
    				continue # Because of those deactivated profiles, different profiles and names were getting mapped so skipping those pages which contain them
    				
    			except:
    				
    				flag=False
    
    
    		names=driver.find_elements_by_css_selector('span.name.actor-name') #Profile name
    		titles= [name.text for name in names]
    		
    			
    
    		headlines=driver.find_elements_by_xpath("//p[@class='subline-level-1 t-14 t-black t-normal search-result__truncate']") #Profile work info ot title
    		heading=[pos.text for pos in headlines]
    		
    			
    
    		Address+=profiles
    		Names+=titles
    		Position+=heading
    
    		try:
    			
    			element=WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//button[@class='artdeco-pagination__button artdeco-pagination__button--next artdeco-button artdeco-button--muted artdeco-button--icon-right artdeco-button--1 artdeco-button--tertiary ember-view']")))
    			element.click()
    			time.sleep(1)
    			print ('\n')
    		except:
    			
    			flag=False

    driver.quit()
    
    
    df= pd.DataFrame({'Name':Names, 'Title':Position, 'Contact': Address})
    df.to_csv('linkedin_profiles.csv', index=False)


if __name__=="__main__":
    username=input("Linkedin username: ")
    password=input("Linkedin Password: ")
    getprofiles(username, password)


	